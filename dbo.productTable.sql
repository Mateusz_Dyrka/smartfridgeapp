﻿CREATE TABLE [dbo].[productTable]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [Code] VARCHAR(50) NULL, 
    [Amount] VARCHAR(50) NOT NULL, 
    [Price] VARCHAR(50) NULL
)
