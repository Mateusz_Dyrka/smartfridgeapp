﻿namespace smartFridge
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelInside = new System.Windows.Forms.Panel();
            this.displayBtn = new System.Windows.Forms.Button();
            this.dataGridViewContent = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.panelDeleteProd = new System.Windows.Forms.Panel();
            this.deleteAmountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.deleteAmountBtn = new System.Windows.Forms.Button();
            this.deleteUnitLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnDeleteProduct = new System.Windows.Forms.Button();
            this.deleteProdCodeTxtBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelAdd = new System.Windows.Forms.Panel();
            this.addAmountBtn = new System.Windows.Forms.Button();
            this.ExpirationDateTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.unitComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.priceTxtBox = new System.Windows.Forms.TextBox();
            this.amountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.nameTxtBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.codeTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelExpProd = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.periodCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.showExpProducts = new System.Windows.Forms.Button();
            this.expiredProductTxtBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSendSMS = new System.Windows.Forms.Button();
            this.dateLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.deleteNameLabel = new System.Windows.Forms.Label();
            this.panelInside.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewContent)).BeginInit();
            this.panelDeleteProd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteAmountNumericUpDown)).BeginInit();
            this.panelAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.amountNumericUpDown)).BeginInit();
            this.panelExpProd.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelInside
            // 
            this.panelInside.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelInside.Controls.Add(this.displayBtn);
            this.panelInside.Controls.Add(this.dataGridViewContent);
            this.panelInside.Controls.Add(this.label1);
            this.panelInside.Location = new System.Drawing.Point(12, 16);
            this.panelInside.Name = "panelInside";
            this.panelInside.Size = new System.Drawing.Size(299, 285);
            this.panelInside.TabIndex = 0;
            // 
            // displayBtn
            // 
            this.displayBtn.Location = new System.Drawing.Point(107, 253);
            this.displayBtn.Name = "displayBtn";
            this.displayBtn.Size = new System.Drawing.Size(91, 23);
            this.displayBtn.TabIndex = 2;
            this.displayBtn.Text = "Pokaż/Odśwież ";
            this.displayBtn.UseVisualStyleBackColor = true;
            this.displayBtn.Click += new System.EventHandler(this.displayBtn_Click);
            // 
            // dataGridViewContent
            // 
            this.dataGridViewContent.AllowUserToAddRows = false;
            this.dataGridViewContent.AllowUserToDeleteRows = false;
            this.dataGridViewContent.AllowUserToResizeColumns = false;
            this.dataGridViewContent.AllowUserToResizeRows = false;
            this.dataGridViewContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewContent.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewContent.Name = "dataGridViewContent";
            this.dataGridViewContent.ReadOnly = true;
            this.dataGridViewContent.Size = new System.Drawing.Size(299, 285);
            this.dataGridViewContent.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(79, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ZAWARTOŚĆ LODÓWKI";
            // 
            // panelDeleteProd
            // 
            this.panelDeleteProd.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelDeleteProd.Controls.Add(this.deleteNameLabel);
            this.panelDeleteProd.Controls.Add(this.deleteAmountNumericUpDown);
            this.panelDeleteProd.Controls.Add(this.deleteAmountBtn);
            this.panelDeleteProd.Controls.Add(this.deleteUnitLabel);
            this.panelDeleteProd.Controls.Add(this.label14);
            this.panelDeleteProd.Controls.Add(this.btnDeleteProduct);
            this.panelDeleteProd.Controls.Add(this.deleteProdCodeTxtBox);
            this.panelDeleteProd.Controls.Add(this.label12);
            this.panelDeleteProd.Controls.Add(this.label11);
            this.panelDeleteProd.Controls.Add(this.label9);
            this.panelDeleteProd.Controls.Add(this.label3);
            this.panelDeleteProd.Location = new System.Drawing.Point(622, 16);
            this.panelDeleteProd.Name = "panelDeleteProd";
            this.panelDeleteProd.Size = new System.Drawing.Size(299, 285);
            this.panelDeleteProd.TabIndex = 4;
            // 
            // deleteAmountNumericUpDown
            // 
            this.deleteAmountNumericUpDown.Location = new System.Drawing.Point(110, 169);
            this.deleteAmountNumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.deleteAmountNumericUpDown.Name = "deleteAmountNumericUpDown";
            this.deleteAmountNumericUpDown.Size = new System.Drawing.Size(144, 20);
            this.deleteAmountNumericUpDown.TabIndex = 27;
            // 
            // deleteAmountBtn
            // 
            this.deleteAmountBtn.Enabled = false;
            this.deleteAmountBtn.Location = new System.Drawing.Point(110, 223);
            this.deleteAmountBtn.Name = "deleteAmountBtn";
            this.deleteAmountBtn.Size = new System.Drawing.Size(144, 23);
            this.deleteAmountBtn.TabIndex = 25;
            this.deleteAmountBtn.Text = "Zmień ilość";
            this.deleteAmountBtn.UseVisualStyleBackColor = true;
            this.deleteAmountBtn.Click += new System.EventHandler(this.deleteAmountBtn_Click);
            // 
            // deleteUnitLabel
            // 
            this.deleteUnitLabel.AutoSize = true;
            this.deleteUnitLabel.Location = new System.Drawing.Point(107, 198);
            this.deleteUnitLabel.Name = "deleteUnitLabel";
            this.deleteUnitLabel.Size = new System.Drawing.Size(13, 13);
            this.deleteUnitLabel.TabIndex = 24;
            this.deleteUnitLabel.Text = "[]";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 196);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Jednostka:";
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.Enabled = false;
            this.btnDeleteProduct.Location = new System.Drawing.Point(110, 121);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(144, 23);
            this.btnDeleteProduct.TabIndex = 19;
            this.btnDeleteProduct.Text = "Usuń produkt";
            this.btnDeleteProduct.UseVisualStyleBackColor = true;
            this.btnDeleteProduct.Click += new System.EventHandler(this.btnDeleteProduct_Click);
            // 
            // deleteProdCodeTxtBox
            // 
            this.deleteProdCodeTxtBox.Location = new System.Drawing.Point(110, 38);
            this.deleteProdCodeTxtBox.Name = "deleteProdCodeTxtBox";
            this.deleteProdCodeTxtBox.Size = new System.Drawing.Size(144, 20);
            this.deleteProdCodeTxtBox.TabIndex = 21;
            this.deleteProdCodeTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deleteProdCodeTxtBox_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 173);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Ilość:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Nazwa:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Wprowadź kod:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(103, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "USUŃ PRODUKT";
            // 
            // panelAdd
            // 
            this.panelAdd.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelAdd.Controls.Add(this.addAmountBtn);
            this.panelAdd.Controls.Add(this.ExpirationDateTime);
            this.panelAdd.Controls.Add(this.label6);
            this.panelAdd.Controls.Add(this.unitComboBox);
            this.panelAdd.Controls.Add(this.label13);
            this.panelAdd.Controls.Add(this.btnAddProduct);
            this.panelAdd.Controls.Add(this.priceTxtBox);
            this.panelAdd.Controls.Add(this.amountNumericUpDown);
            this.panelAdd.Controls.Add(this.nameTxtBox);
            this.panelAdd.Controls.Add(this.label10);
            this.panelAdd.Controls.Add(this.label8);
            this.panelAdd.Controls.Add(this.label7);
            this.panelAdd.Controls.Add(this.label5);
            this.panelAdd.Controls.Add(this.codeTxtBox);
            this.panelAdd.Controls.Add(this.label2);
            this.panelAdd.Location = new System.Drawing.Point(317, 16);
            this.panelAdd.Name = "panelAdd";
            this.panelAdd.Size = new System.Drawing.Size(299, 285);
            this.panelAdd.TabIndex = 5;
            // 
            // addAmountBtn
            // 
            this.addAmountBtn.Enabled = false;
            this.addAmountBtn.Location = new System.Drawing.Point(65, 253);
            this.addAmountBtn.Name = "addAmountBtn";
            this.addAmountBtn.Size = new System.Drawing.Size(188, 23);
            this.addAmountBtn.TabIndex = 23;
            this.addAmountBtn.Text = "Zmień ilość";
            this.addAmountBtn.UseVisualStyleBackColor = true;
            this.addAmountBtn.Click += new System.EventHandler(this.addAmountBtn_Click);
            // 
            // ExpirationDateTime
            // 
            this.ExpirationDateTime.Location = new System.Drawing.Point(100, 191);
            this.ExpirationDateTime.Name = "ExpirationDateTime";
            this.ExpirationDateTime.Size = new System.Drawing.Size(161, 20);
            this.ExpirationDateTime.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Data ważności:";
            // 
            // unitComboBox
            // 
            this.unitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.unitComboBox.FormattingEnabled = true;
            this.unitComboBox.Items.AddRange(new object[] {
            "sztuk(a)",
            "dekagram",
            "mililitr"});
            this.unitComboBox.Location = new System.Drawing.Point(100, 164);
            this.unitComboBox.Name = "unitComboBox";
            this.unitComboBox.Size = new System.Drawing.Size(120, 21);
            this.unitComboBox.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 167);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Jednostka:";
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Enabled = false;
            this.btnAddProduct.Location = new System.Drawing.Point(65, 223);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(188, 23);
            this.btnAddProduct.TabIndex = 18;
            this.btnAddProduct.Text = "Dodaj";
            this.btnAddProduct.UseVisualStyleBackColor = true;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // priceTxtBox
            // 
            this.priceTxtBox.Location = new System.Drawing.Point(100, 110);
            this.priceTxtBox.Name = "priceTxtBox";
            this.priceTxtBox.Size = new System.Drawing.Size(100, 20);
            this.priceTxtBox.TabIndex = 17;
            this.priceTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceTxtBox_KeyPress);
            // 
            // amountNumericUpDown
            // 
            this.amountNumericUpDown.Location = new System.Drawing.Point(100, 140);
            this.amountNumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.amountNumericUpDown.Name = "amountNumericUpDown";
            this.amountNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.amountNumericUpDown.TabIndex = 16;
            // 
            // nameTxtBox
            // 
            this.nameTxtBox.Location = new System.Drawing.Point(100, 82);
            this.nameTxtBox.Name = "nameTxtBox";
            this.nameTxtBox.Size = new System.Drawing.Size(100, 20);
            this.nameTxtBox.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Ilość:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Cena:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Nazwa:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Wprowadź kod:";
            // 
            // codeTxtBox
            // 
            this.codeTxtBox.Location = new System.Drawing.Point(99, 42);
            this.codeTxtBox.Name = "codeTxtBox";
            this.codeTxtBox.Size = new System.Drawing.Size(188, 20);
            this.codeTxtBox.TabIndex = 1;
            this.codeTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.codeTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.codeTxtBox_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(104, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "DODAJ PRODUKT";
            // 
            // panelExpProd
            // 
            this.panelExpProd.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelExpProd.Controls.Add(this.label15);
            this.panelExpProd.Controls.Add(this.periodCheckedListBox);
            this.panelExpProd.Controls.Add(this.showExpProducts);
            this.panelExpProd.Controls.Add(this.expiredProductTxtBox);
            this.panelExpProd.Controls.Add(this.label4);
            this.panelExpProd.Controls.Add(this.btnSendSMS);
            this.panelExpProd.Location = new System.Drawing.Point(189, 307);
            this.panelExpProd.Name = "panelExpProd";
            this.panelExpProd.Size = new System.Drawing.Size(553, 129);
            this.panelExpProd.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(44, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "OKRES WAŻNOŚCI";
            // 
            // periodCheckedListBox
            // 
            this.periodCheckedListBox.FormattingEnabled = true;
            this.periodCheckedListBox.Items.AddRange(new object[] {
            "1 dzień",
            "3 dni",
            "7 dni"});
            this.periodCheckedListBox.Location = new System.Drawing.Point(61, 35);
            this.periodCheckedListBox.Name = "periodCheckedListBox";
            this.periodCheckedListBox.Size = new System.Drawing.Size(70, 79);
            this.periodCheckedListBox.TabIndex = 6;
            this.periodCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.periodCheckedListBox_ItemCheck);
            // 
            // showExpProducts
            // 
            this.showExpProducts.Enabled = false;
            this.showExpProducts.Location = new System.Drawing.Point(451, 35);
            this.showExpProducts.Name = "showExpProducts";
            this.showExpProducts.Size = new System.Drawing.Size(75, 23);
            this.showExpProducts.TabIndex = 5;
            this.showExpProducts.Text = "Pokaż";
            this.showExpProducts.UseVisualStyleBackColor = true;
            this.showExpProducts.Click += new System.EventHandler(this.showExpProducts_Click);
            // 
            // expiredProductTxtBox
            // 
            this.expiredProductTxtBox.Location = new System.Drawing.Point(229, 32);
            this.expiredProductTxtBox.Multiline = true;
            this.expiredProductTxtBox.Name = "expiredProductTxtBox";
            this.expiredProductTxtBox.ReadOnly = true;
            this.expiredProductTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.expiredProductTxtBox.Size = new System.Drawing.Size(168, 82);
            this.expiredProductTxtBox.TabIndex = 4;
            this.expiredProductTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(183, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(291, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "PRODUKTY Z KOŃCZĄCĄ SIĘ DATĄ WAŻNOŚCI";
            // 
            // btnSendSMS
            // 
            this.btnSendSMS.Enabled = false;
            this.btnSendSMS.Location = new System.Drawing.Point(451, 91);
            this.btnSendSMS.Name = "btnSendSMS";
            this.btnSendSMS.Size = new System.Drawing.Size(75, 23);
            this.btnSendSMS.TabIndex = 1;
            this.btnSendSMS.Text = "Wyślij SMS";
            this.btnSendSMS.UseVisualStyleBackColor = true;
            this.btnSendSMS.Click += new System.EventHandler(this.btnSendSMS_Click);
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateLabel.Location = new System.Drawing.Point(797, 358);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(63, 13);
            this.dateLabel.TabIndex = 8;
            this.dateLabel.Text = "dateLabel";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(780, 339);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Dzisiejsza data:";
            // 
            // deleteNameLabel
            // 
            this.deleteNameLabel.AutoSize = true;
            this.deleteNameLabel.Location = new System.Drawing.Point(107, 85);
            this.deleteNameLabel.Name = "deleteNameLabel";
            this.deleteNameLabel.Size = new System.Drawing.Size(13, 13);
            this.deleteNameLabel.TabIndex = 28;
            this.deleteNameLabel.Text = "[]";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(932, 448);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.panelExpProd);
            this.Controls.Add(this.panelAdd);
            this.Controls.Add(this.panelDeleteProd);
            this.Controls.Add(this.panelInside);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Smart Fridge";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelInside.ResumeLayout(false);
            this.panelInside.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewContent)).EndInit();
            this.panelDeleteProd.ResumeLayout(false);
            this.panelDeleteProd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteAmountNumericUpDown)).EndInit();
            this.panelAdd.ResumeLayout(false);
            this.panelAdd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.amountNumericUpDown)).EndInit();
            this.panelExpProd.ResumeLayout(false);
            this.panelExpProd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelInside;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelDeleteProd;
        private System.Windows.Forms.Panel panelAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelExpProd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox codeTxtBox;
        private System.Windows.Forms.TextBox nameTxtBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown amountNumericUpDown;
        private System.Windows.Forms.TextBox priceTxtBox;
        private System.Windows.Forms.DataGridView dataGridViewContent;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.Button btnDeleteProduct;
        private System.Windows.Forms.TextBox deleteProdCodeTxtBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox unitComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label deleteUnitLabel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button deleteAmountBtn;
        private System.Windows.Forms.NumericUpDown deleteAmountNumericUpDown;
        private System.Windows.Forms.Button btnSendSMS;
        private System.Windows.Forms.DateTimePicker ExpirationDateTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button showExpProducts;
        private System.Windows.Forms.TextBox expiredProductTxtBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckedListBox periodCheckedListBox;
        private System.Windows.Forms.Button addAmountBtn;
        private System.Windows.Forms.Button displayBtn;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label deleteNameLabel;
    }
}

