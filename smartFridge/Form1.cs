﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using Twilio;

namespace smartFridge
{
    public partial class Form1 : Form
    {
        bool decision;
        smartFridgeDBEntities context = new smartFridgeDBEntities();
        ProductInFridge deletedProduct = new ProductInFridge();
        int oldDeleteAmount;
        int oldAddAmount = 0;
        int expirationPeriod = 0;
        


        public Form1()
        {
            InitializeComponent();
            panelDeleteProd.Visible = true;
            panelAdd.Visible = true;
            panelExpProd.Visible = true;
            dateLabel.Text = DateTime.Today.ToShortDateString();
            
            //displayContent();
        }

        private void btnContent_Click(object sender, EventArgs e)
        {
            panelInside.Visible = true;
            panelExpProd.Visible = false;
            panelDeleteProd.Visible = false;
            panelAdd.Visible = false;
            cleanAddBoxes();
            cleanDeleteBoxes();
            cleanMessageBoxes();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            panelAdd.Visible = true;
            panelExpProd.Visible = false;
            panelDeleteProd.Visible = false;
            cleanAddBoxes();
            cleanDeleteBoxes();
            cleanMessageBoxes();
        }
        private void btnCookbook_Click(object sender, EventArgs e)
        {
            panelDeleteProd.Visible = true;
            panelExpProd.Visible = false;
            panelAdd.Visible = false;
            cleanAddBoxes();
            cleanDeleteBoxes();
            cleanMessageBoxes();
        }
        private void expProdBtn_Click(object sender, EventArgs e)
        {
            panelDeleteProd.Visible = false;
            panelExpProd.Visible = true;
            panelAdd.Visible = false;
            cleanAddBoxes();
            cleanDeleteBoxes();
            cleanMessageBoxes();
        }
        public bool getProductData()
        {
            if (string.IsNullOrWhiteSpace(codeTxtBox.Text))
            {
                alertMessageBox("Wprowadź kod produktu.");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(nameTxtBox.Text))
            {
                alertMessageBox("Wprowadz nazwę produktu.");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(amountNumericUpDown.Text))
            {
                alertMessageBox("Wprowadz ilość produktów.");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(priceTxtBox.Text))
            {
                alertMessageBox("Wprowadz cenę produktu.");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(unitComboBox.Text))
            {
                alertMessageBox("Wprowadz jednostkę produktu.");
                return false;
            }
            else
            {
                Console.WriteLine("Wprowadzono wszystkie dane", "Uwaga!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return true;
            }
        }
        public void cleanAddBoxes()
        {
            nameTxtBox.Clear();
            amountNumericUpDown.ResetText();
            priceTxtBox.Clear();
            unitComboBox.ResetText();
            amountNumericUpDown.BackColor = Color.Empty;
            unitComboBox.BackColor = Color.Empty;
            ExpirationDateTime.Value = DateTime.Today;
        }
        public void cleanMessageBoxes()
        {
            periodCheckedListBox.ClearSelected();
            expiredProductTxtBox.Clear();
        }
        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            barcodeChecked(decision);
        }

        private ProductInFridge addProductToFridge(String bcode, String amount, String unit, DateTime data)
        {
            ProductInFridge product = new ProductInFridge
            {
                BarcodeNumber = bcode,
                Amount = amount,
                Unit = unit,
                DateOfPlacing = DateTime.Today,
                DateExpiration = data
            };
            return product;
        }
        private Product addProduct(String prodName, String prodBarcode, String prodPrice)
        {
            Product product = new Product
            {
                Name = prodName,
                Barcode = prodBarcode,
                Price = prodPrice
            };
            return product;
        }

        private void enableBoxes(int condition)
        {
            switch (condition)
            {
                case 1:                         //is in product - add
                    nameTxtBox.Enabled = false;
                    priceTxtBox.Enabled = false;
                    btnAddProduct.Enabled = true;
                    break;

                case 2:                         //is in fridge - add 
                    unitComboBox.Enabled = false;
                    nameTxtBox.Enabled = false;
                    priceTxtBox.Enabled = false;
                    btnAddProduct.Enabled = true;
                    addAmountBtn.Enabled = true;
                    break;

                case 3:                         //is in fridge - delete
                    btnDeleteProduct.Enabled = true;
                    deleteAmountBtn.Enabled = true;
                    break;
            }
        }

        private bool checkBarcode()
        {
            var prodInFridge = findProductInFridge(codeTxtBox.Text);
            if (prodInFridge != null)
            {
                setAddBoxes(prodInFridge);
                enableBoxes(2);
                return true;
            }
            else
            {
                var prod = context.Products.Find(codeTxtBox.Text);                                      //używam PK
                if (prod == null)
                {
                    alertMessageBox("Nie znaleziono podanego kodu kreskowego, uzupełnij resztę danych.");
                    cleanAddBoxes();
                    return false;
                }
                else
                {
                    setAddBoxes(prod);
                    enableBoxes(1);
                    return true;
                }
            }
        }

        private void codeTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                decision = checkBarcode();
            }
        }

        private void barcodeChecked(bool decision)
        {
            if (decision)
            {
                if (amountNumericUpDown.Value != 0)
                {
                    context.ProductInFridges.Add(addProductToFridge(codeTxtBox.Text, amountNumericUpDown.Value.ToString(), unitComboBox.Text, ExpirationDateTime.Value));
                    informationMessageBox("Dodano do lodówki.");
                    cleanAddBoxes();
                    codeTxtBox.Clear();
                }
                else
                {
                    alertMessageBox("Dodaj ilość.");
                }
            }
            else
            {
                if (getProductData())
                {
                    context.Products.Add(addProduct(nameTxtBox.Text, codeTxtBox.Text, priceTxtBox.Text));
                    context.ProductInFridges.Add(addProductToFridge(codeTxtBox.Text, amountNumericUpDown.Value.ToString(), unitComboBox.Text, ExpirationDateTime.Value));         //prod.barcode czy codeTxtBox.text
                    informationMessageBox("Dodano do bazy danych produktów oraz do lodówki.");
                    cleanAddBoxes();
                    codeTxtBox.Clear();
                }
                else
                {
                    alertMessageBox("uzupełnij dane produktu.");
                }
            }
            context.SaveChanges();
        }

        private void alertMessageBox(string alert)
        {
            MessageBox.Show(alert, 
                "Uwaga!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button1);
        }
        private void informationMessageBox(string info)
        {
            MessageBox.Show(info,
                "Informacja!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1);
        }
        private ProductInFridge findProductInFridge(String barcode)
        {
            var prod = context.ProductInFridges.
                Where(p => p.BarcodeNumber == barcode).
                FirstOrDefault();                                                       //używam query
            if (prod == null)
            {
                return null;
            }
            else
            {
                return prod;
            }
        }
        void setAddBoxes(ProductInFridge prod)
        {
            nameTxtBox.Text = prod.Product.Name;
            priceTxtBox.Text = prod.Product.Price;
            amountNumericUpDown.Text = prod.Amount;
            unitComboBox.Text = prod.Unit;
            ExpirationDateTime.Value = prod.DateExpiration;
            unitComboBox.BackColor = Color.Empty;
            amountNumericUpDown.BackColor = Color.Empty;
            oldAddAmount = getAmountValue(amountNumericUpDown.Value.ToString());
        }
        void setAddBoxes(Product prod)
        {
            nameTxtBox.Text = prod.Name;
            priceTxtBox.Text = prod.Price;
            amountNumericUpDown.Value = 0;
            amountNumericUpDown.BackColor = Color.Red;
            unitComboBox.Text = "";
            unitComboBox.BackColor = Color.Red;
            ExpirationDateTime.Value = DateTime.Today;
        }
        void setDeleteBoxes(ProductInFridge prod)
        {
            deleteProdCodeTxtBox.Text = prod.BarcodeNumber;
            deleteNameLabel.Text = prod.Product.Name;
            deleteAmountNumericUpDown.Text = prod.Amount;
            deleteUnitLabel.Text = prod.Unit;
        }
        void cleanDeleteBoxes()
        {
            deleteProdCodeTxtBox.Clear();
            deleteNameLabel.Text = "[]";
            deleteAmountNumericUpDown.ResetText();
            deleteUnitLabel.Text = "[]";
        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(deleteProdCodeTxtBox.Text))
            {
                alertMessageBox("Wprowadz kod produktu, który chcesz usunać.");
                deleteProdCodeTxtBox.BackColor = Color.Red;
            }
            else
            {
                context.ProductInFridges.Remove(deletedProduct);
                context.SaveChanges();
                informationMessageBox("Usunięto produkt.");
                cleanDeleteBoxes();
            }
        }

        private void deleteProdCodeTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var delProduct = findProductInFridge(deleteProdCodeTxtBox.Text);
                if (delProduct != null)
                {
                    deletedProduct = delProduct;
                    setDeleteBoxes(delProduct);
                    oldDeleteAmount = getAmountValue(deleteAmountNumericUpDown.Value.ToString());
                    deleteProdCodeTxtBox.BackColor = Color.Empty;
                    enableBoxes(3);
                }
                else
                {
                    alertMessageBox("Nie ma takiego produktu.");
                    cleanDeleteBoxes();
                    deleteProdCodeTxtBox.BackColor = Color.Red;
                }
            }
        }

        private int getAmountValue(string amount)
        {
            int value = int.Parse(amount);
            return value;
        }

        private void deleteAmountBtn_Click(object sender, EventArgs e)
        {
            int newAmount = getAmountValue(deleteAmountNumericUpDown.Value.ToString());
            if (newAmount > oldDeleteAmount)
            {
                alertMessageBox("Nowa ilość musi być mniejsza.");
                deleteAmountNumericUpDown.BackColor = Color.Red;
            }
            else if (newAmount == oldDeleteAmount)
            {
                if (string.IsNullOrWhiteSpace(deleteProdCodeTxtBox.Text))
                {
                    alertMessageBox("Wprowadz kod produktu.");
                }
                else
                {
                    alertMessageBox("Zmień ilość.");
                    deleteAmountNumericUpDown.BackColor = Color.Red;
                }
            }
            else
            {
                var updatedProduct = findProductInFridge(deleteProdCodeTxtBox.Text);
                updatedProduct.Amount = newAmount.ToString();
                context.SaveChanges();
                deleteAmountNumericUpDown.BackColor = Color.Empty;
                informationMessageBox("Ilość została zmieniona.");
                cleanDeleteBoxes();
            }
        }

        private void sendSMS()
        {
            string text = "W okresie " + expirationPeriod + " dni, następujące produkty ulegną przeterminowaniu:" 
                        + System.Environment.NewLine + expiredProductTxtBox.Text;
            string AccountSid = "ACd1ab8990c3a6bbfb9da8bcd0d2b705b8";
            string AuthToken = "942162d650e60d97f65c0c0200126f62";
            var twilio = new TwilioRestClient(AccountSid, AuthToken);

            var message = twilio.SendMessage(
                "+48732483848",
                "+48600945873",
                text
            );
            Console.WriteLine(message.Sid);
        }

        private void btnSendSMS_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(expiredProductTxtBox.Text))
            {

                Console.WriteLine("Wysyłam!");
                sendSMS();
                cleanMessageBoxes();
            }
            else
            {
                informationMessageBox("Ani jeden produkt nie ulega przeterminowaniu w podanym okresie czasu.");
            }
        }

        private void getExpiredProducts(double periodOfTime)    
        {
            DateTime deadlineDate;
            deadlineDate = DateTime.Today.AddDays(periodOfTime);
            var expiredProd = context.ProductInFridges.
                                Where(e => e.DateExpiration <= deadlineDate).
                                ToList();
            if(expiredProd.Count==0)
            {
                informationMessageBox("Nie ma produktów, których data ważnosci kończy się w podanym okresie.");
            }
            else
            {
                foreach (var product in expiredProd)
                {
                    expiredProductTxtBox.AppendText(product.Product.Name + System.Environment.NewLine +
                                                    "data ważności: " + product.DateExpiration.ToShortDateString() + System.Environment.NewLine);
                }
                btnSendSMS.Enabled = true;
            }
            
        }

        private void showExpProducts_Click(object sender, EventArgs e)
        {
            if (periodCheckedListBox.CheckedItems.Count == 0)
            {
                alertMessageBox("Wybierz okres czasu, który chcesz sprawdzić.");
            }
            else
            {
                expiredProductTxtBox.Clear();
                getExpiredProducts(expirationPeriod);
                Console.WriteLine("Ma pokazać oraz okres wynosi: " + expirationPeriod);
            }
        }

        private void periodCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            showExpProducts.Enabled = true;
            for (int ix = 0; ix < periodCheckedListBox.Items.Count; ++ix)
                if (ix != e.Index) periodCheckedListBox.SetItemChecked(ix, false);

            string currentItem = (string)this.periodCheckedListBox.Items[e.Index];
            Console.WriteLine("Current Item: " + currentItem);
            if (currentItem == "1 dzień")
            {
                expirationPeriod = 1;
            }
            else if (currentItem == "3 dni")
            {
                expirationPeriod = 3;
            }
            else if (currentItem == "7 dni")
            {
                expirationPeriod = 7;
            }
        }

        private void addAmountBtn_Click(object sender, EventArgs e)
        {
            int newAmount = getAmountValue(amountNumericUpDown.Value.ToString());
            if (oldAddAmount != 0)
            {
                if (newAmount < oldAddAmount)
                {
                    alertMessageBox("Nowa ilość musi być większa.");
                    amountNumericUpDown.BackColor = Color.Red;
                }
                else if (newAmount == oldAddAmount)
                {
                    alertMessageBox("Zmień ilość.");
                    amountNumericUpDown.BackColor = Color.Red;
                }
                else
                {
                    var updatedProduct = findProductInFridge(codeTxtBox.Text);
                    updatedProduct.Amount = newAmount.ToString();
                    context.SaveChanges();
                    amountNumericUpDown.BackColor = Color.Empty;
                    informationMessageBox("Ilość została zmieniona.");
                }
            }
            else
            {
                alertMessageBox("Nie da się zmienić ilości, bo dany produkt nie znajduje się w lodówce");
            }
        }

        void displayContent()
        {
            DateTime today;
            today = DateTime.Today;
            
            var query = from p in context.ProductInFridges
                        orderby p.DateExpiration
                        select new { p.Product.Name, p.Amount, p.Unit, p.DateExpiration, };
            var results = query.ToList();
            dataGridViewContent.DataSource = results;

            foreach (DataGridViewRow row in dataGridViewContent.Rows)
            {
                DateTime date = (DateTime) row.Cells["DateExpiration"].Value;
                if (date <= today)
                {
                    row.DefaultCellStyle.BackColor = Color.Red;
                }
            }
        }

        private void displayBtn_Click(object sender, EventArgs e)
        {
            displayContent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void priceTxtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }




    }
}
